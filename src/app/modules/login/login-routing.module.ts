import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FacebookComponent } from './facebook/facebook.component';
import { TwitterComponent } from './twitter/twitter.component';
import { GoogleComponent } from './google/google.component';

const routes: Routes = [
  { path: 'facebook', component: FacebookComponent },
  { path: 'twitter', component: TwitterComponent },
  { path: 'google', component: GoogleComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
