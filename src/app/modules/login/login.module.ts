import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';


import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FacebookComponent } from './facebook/facebook.component';
import { GoogleComponent } from './google/google.component';
import { TwitterComponent } from './twitter/twitter.component';
import { LanguageService } from '../../common/language.service';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/login/', '.json');
}

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      isolate: true
    }),
    LoginRoutingModule
  ],
  declarations: [FacebookComponent, GoogleComponent, TwitterComponent],
  exports: [FacebookComponent, GoogleComponent, TwitterComponent]
})
export class LoginModule {
  constructor(private $translate: TranslateService, private ls: LanguageService) {
    ls.language.subscribe(lang => $translate.use(lang));
  }
}
