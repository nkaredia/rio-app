import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'rio-google',
  templateUrl: './' + environment.brand + '/google.component.html',
  styleUrls: ['./' + environment.brand + '/google.component.scss']
})
export class GoogleComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
