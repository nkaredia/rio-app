import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'rio-facebook',
  templateUrl: './' + environment.brand + '/facebook.component.html',
  styleUrls: ['./' + environment.brand + '/facebook.component.scss']
})
export class FacebookComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
