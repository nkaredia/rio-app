import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'rio-twitter',
  templateUrl: './' + environment.brand + '/twitter.component.html',
  styleUrls: ['./' + environment.brand + '/twitter.component.scss']
})
export class TwitterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
