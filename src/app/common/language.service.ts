import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class LanguageService {

  public readonly language: BehaviorSubject<string>;
  constructor(
    private $translate: TranslateService
  ) {
    this.language = new BehaviorSubject<string>(localStorage.getItem('language') || 'en');
    this.language.subscribe(lang => {
      if (lang === 'en' || lang === 'fr') {
        localStorage.setItem('language', lang);
      }
    });
  }

}
