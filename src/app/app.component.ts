import { Component } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'rio-root',
  templateUrl: './templates/' + environment.brand + '/app.component.html',
  styleUrls: ['./templates/' + environment.brand + '/app.component.scss']
})
export class AppComponent {
  title = 'rio';
  env = environment.brand;
  constructor(private $http: HttpClient) {
    this.getTitle();
  }

  getTitle = () => {
    this.$http.post('/rio/test', {}).subscribe((res: any) => {
      console.log(res);
      this.title = res.msg;
    });
  }
}
