# RioApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.3.

## Installation

Make sure node version is above 6.9.2

npm install -g yarn

After Yarn is installed globally enter following:

yarn

## Development server

- Command

yarn run serve:rogers:local or yarn run serve:fido:local

## Generate Custom Code from blue print 
By default schematics is pointing to @ute/schematics instead of @angular/schematics

- Module
ng g m <path> e.g ng g m modules/newcomp -> this will create src/app/modules/newmod/newmod.module.ts and routing file
Each module generated injects 'TranslateModule' and sets translate configuration by default

- Component
ng g c <path> -m <pathToModule>
Example: ng g c modules/newmod/somecomp -m modules/newmod

C.omponent generated will create a component.ts file and rogers and fido folder with their own html and scss files.
It will also create an import statement in the newmod module