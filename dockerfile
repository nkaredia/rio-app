FROM node:8.9.4
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json /usr/src/app
RUN npm install -g yarn
RUN yarn
COPY . /usr/src/app
EXPOSE 4200
CMD ["yarn", "docker:rogers:local"]